<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }
    
    public function kirim(Request $request){
        $nama1 = $request['nama1'];
        $nama2 = $request['nama2'];
        return view('halaman.home', compact('nama1', 'nama2'));
    }
}
